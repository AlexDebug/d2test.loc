<?php
require_once 'vendor/autoload.php';
use D2\Filters\InsuranceFilter;
use D2\Persons\EasyPerson;
use D2\Controllers\AjaxController;

if ($_POST) {
	try{
		$filter = new InsuranceFilter($_POST);
		$post 	= $filter->execute();

		$person = new EasyPerson($post);

		$ajax 	= new AjaxController($person);
		echo json_encode($ajax->execute());
	}
	catch(\Exception $e) {
		echo json_encode(
			[
				"exception" => true,
				"message"   => $e->getMessage()
			]);
	}
}