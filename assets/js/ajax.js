jQuery(document).on("submit", "#insuranceForm",
    function (e) {
        e.preventDefault();
        let data    = jQuery(this).serialize();
        jQuery("#danger, #success").addClass("d-none");

        jQuery.ajax({
            type: "POST",
            url: "/ajax.php",
            async: false,
            data: data,
            dataType: 'json',
        })
            .done(function (data) {
                console.log(data);
                if (true == data.exception) {
                    jQuery("#danger")
                        .text(data.message)
                        .removeClass("d-none");
                }
                else {
                    jQuery("#success")
                        .html("<a href='" + data + "'>PDF</a>")
                        .removeClass("d-none");
                }
            })
            .fail(function () {
                alert("error");
            })
            .always(function () {

            })
    });