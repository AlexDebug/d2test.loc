<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="/assets/libs/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/common.css">

    <title>D2</title>
  </head>

  <body>
    <h1>D2</h1>


    <form id="insuranceForm">

      <div class="form-group">
        <label for="INSURER_FIRSTNAME">Имя страхователя</label>
        <input class="form-control" id="INSURER_FIRSTNAME" name="INSURER_FIRSTNAME" required />
      </div>

      <div class="form-group">
        <label for="INSURER_SURNAME">Фамилия страхователя</label>
        <input class="form-control" id="INSURER_SURNAME" name="INSURER_SURNAME" required />
      </div>

      <div class="form-group">
        <label for="INSURER_EMAIL">Email</label>
        <input type="email" class="form-control" id="INSURER_EMAIL" name="INSURER_EMAIL" required />
      </div>

      <div class="form-group">
        <label for="INSURER_BIRTHDAY">Дата рождения страхователя</label>
        <input type="date" class="form-control" id="INSURER_BIRTHDAY" name="INSURER_BIRTHDAY" required />
      </div>

      <div class="form-group">
        <label for="INSURER_PHONE">Телефон страхователя</label>
        <input type="tel" placeholder="7xxxxxxxx" pattern="[0-9]{11}" class="form-control" id="INSURER_PHONE" name="INSURER_PHONE" required />
      </div>

      <div class="form-group">
        <label for="DOP_INFO1">Доп. информация</label>
        <input class="form-control" id="DOP_INFO1" name="DOP_INFO1" required />
      </div>

      <div class="form-group">
        <label for="DOP_INFO2">Доп. информация</label>
        <input class="form-control" id="DOP_INFO2" name="DOP_INFO2" required />
      </div>

      <div class="form-group">
        <label for="INSURED1">ФИО застрахованного</label>
        <input class="form-control" id="INSURED1" name="INSURED1" required />
      </div>

      <div class="form-group">
        <label for="INSURED_BIRTHDAY1">Дата рождения застрахованного</label>
        <input type="date" class="form-control" id="INSURED_BIRTHDAY1" name="INSURED_BIRTHDAY1" required />
      </div>

      <button type="submit" class="btn btn-primary">Отправить</button>
    </form>

    <div id="danger" class="alert alert-danger d-none"></div>
    <div id="success" class="alert alert-success d-none"></div>

    <script src="/assets/libs/jquery.js"></script>
    <script src="/assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="/assets/js/ajax.js"></script>

  </body>
</html>