<?php
namespace D2\Controllers;
use D2\Persons\EasyPerson;
use D2\Polices\GetPolicy;
/**
 *
 */
class AjaxController
{
	protected $person;

	function __construct(EasyPerson $person)
	{
		$this->person = $person;
	}

	public function execute()
	{
		$getPolicy = new GetPolicy($this->person);
		return $getPolicy->execute();
	}
};