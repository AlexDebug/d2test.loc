<?php
namespace D2\Filters;
/**
 *
 */
class InsuranceFilter
{
	protected $data;

	function __construct(array $data)
	{
		$this->data = $data;
	}

	public function execute() : array
	{
		if ($this->filter()
			 ->checkDataCompleteness())
			return $this->data;

		return [];
	}

	protected function filter() : self
	{
		$result 	= array();
		$strings 	= [
			"INSURER_FIRSTNAME" => $this->data["INSURER_FIRSTNAME"],
			"INSURER_SURNAME" 	=> $this->data["INSURER_SURNAME"],
			"DOP_INFO1"			=> $this->data["DOP_INFO1"],
			"DOP_INFO2" 		=> $this->data["DOP_INFO2"],
			"INSURED1" 			=> $this->data["INSURED1"],
		];

		$emails 	= [
			"INSURER_EMAIL" 	=> $this->data["INSURER_EMAIL"]
		];

		$dates = [
			"INSURED_BIRTHDAY1"	=> $this->data["INSURED_BIRTHDAY1"],
			"INSURER_BIRTHDAY"	=> $this->data["INSURER_BIRTHDAY"],
		];

		$phones = [
			"INSURER_PHONE"		=> $this->data["INSURER_PHONE"],
		];

		$strings 	= filter_var_array($strings, FILTER_SANITIZE_STRING);
		$emails   	= filter_var_array($emails, FILTER_VALIDATE_EMAIL);
		$dates		= $this->checkDates($dates);
		$phones		= $this->checkPhones($phones);

		$this->data = array_merge($strings, $emails, $dates, $phones);

		return $this;
	}

	protected function checkPhones(array $phones) :array
	{
		foreach ($phones as &$phone) {
			$phone = (preg_match("/^[0-9]{11}$/", $phone)) ? $phone : false;
		}
		unset($phone);

		return $phones;
	}

	protected function checkDates(array $dates) : array
	{
		foreach ($dates as &$date) {
			$check 	= \Datetime::createFromFormat('Y-m-d', $date);
			$date 	= ($check) ? $date : false;
		}
		unset($date);

		return $dates;
	}

	protected function checkDataCompleteness() : bool
	{
		if (!$this->data)
			throw new \Exception("Отсутствуют данные");

		if (!$this->data["INSURER_FIRSTNAME"])
			throw new \Exception("Отсутствует имя страхователя");
		if (!$this->data["INSURER_SURNAME"])
			throw new \Exception("Отсутствует фамилия страхователя");
		if (!$this->data["DOP_INFO1"] || !$this->data["DOP_INFO2"])
			throw new \Exception("Отсутствует доп. информация");
		if (!$this->data["INSURED1"])
			throw new \Exception("Отсутствует ФИО застрахованного");
		if (!$this->data["INSURER_EMAIL"])
			throw new \Exception("Отсутствует email");

		return true;
	}
}