<?php
namespace D2\Persons;
class EasyPerson
{
	public $INSURER_FIRSTNAME;
	public $INSURER_SURNAME;
	public $INSURER_EMAIL;
	public $INSURER_BIRTHDAY;
	public $INSURER_PHONE;
	public $DOP_INFO1;
	public $DOP_INFO2;
	public $INSURED1;
	public $INSURED_BIRTHDAY1;

	function __construct(array $personData)
	{
		$this->INSURER_FIRSTNAME 	= $personData["INSURER_FIRSTNAME"];
	 	$this->INSURER_SURNAME 		= $personData["INSURER_SURNAME"];
	 	$this->INSURER_EMAIL		= $personData["INSURER_EMAIL"];
	 	$this->INSURER_BIRTHDAY		= $personData["INSURER_BIRTHDAY"];
	 	$this->INSURER_PHONE		= $personData["INSURER_PHONE"];
	 	$this->DOP_INFO1			= $personData["DOP_INFO1"];
	 	$this->DOP_INFO2			= $personData["DOP_INFO2"];
	 	$this->INSURED1				= $personData["INSURED1"];
	 	$this->INSURED_BIRTHDAY1	= $personData["INSURED_BIRTHDAY1"];
	}
}