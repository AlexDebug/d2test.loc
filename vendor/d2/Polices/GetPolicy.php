<?php
namespace D2\Polices;
use D2\Persons\EasyPerson;
/**
 *
 */
class GetPolicy
{
	protected $person;
	protected $soapClient;

	const LOGIN 			= "testForUser";
	const PASSWORD 			= "testUser514";
	const PRODUCT_ID 		= "2521268361";
	const APPLICATION_ID 	= "12345678";

	function __construct(EasyPerson $person)
	{
		$this->person 		= $person;
		$this->soapClient 	= new \SoapClient("https://soapdev.d2insur.ru/webservice/PolicyService.wsdl",
			[
				'login'		=> self::LOGIN,
		  		'password'	=> self::PASSWORD,
		  		"productId"	=> self::PRODUCT_ID
		  	]);
	}

	public function execute()
	{
		$params = array(
			"applicationId" 	=> self::APPLICATION_ID,
			"productId"			=> self::PRODUCT_ID,
			"person"			=> $this->person
		);

		if ($response = $this->soapClient->obtainCertificate($params)) {
			if ("OK" != $response->result->code)
				throw new \Exception($response->result->errorDescr);
			else {
				$filePath 	= $_SERVER["DOCUMENT_ROOT"] . "/pdf/{$response->cert->number}.pdf";
				$fileCreate =  file_put_contents($filePath, hex2bin($response->cert->certFile));

				if (false === $fileCreate)
					throw new \Exception("Ошибка при создании файла");
				else
					return $filePath;
			}
		}
	}
}